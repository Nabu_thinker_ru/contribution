// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Contribution {
    address owner;

    mapping(address => uint256) contributions;
    address[] contributors;

    constructor() {
        owner = msg.sender;
    }

    receive() external payable {
        require(msg.value > 0, "Contribution has to be greater than zero");
        if (contributions[msg.sender] == 0) {
            contributors.push(msg.sender);
        }
        contributions[msg.sender] += msg.value;
    }

    function get_all_contributor() public view returns (address[] memory) {
        return contributors;
    }

    function get_contributor_total_sum(address _address)
        public
        view
        returns (uint256)
    {
        return contributions[_address];
    }

    function withdraw(address _address, uint256 _amount) public {
        require(msg.sender == owner, "Only owner can withdraw money");
        require(
            _amount <= address(this).balance,
            "The amount of withdraw is exceeded the balance."
        );
        payable(_address).transfer(_amount);
    }
}
