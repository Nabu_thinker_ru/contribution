const { types } = require("hardhat/config");
const { getContributionContract } = require("../utils/utils");

task("contribute", "Make a contribution")
  .addParam("contract", "The contract address", undefined, types.address)
  .addParam("amount", "Amount for contribute", undefined, types.int)
  .setAction(async (taskArgs, hre) => {
    const { contract, amount } = taskArgs;
    [owner] = await ethers.getSigners();
    const contribution = await getContributionContract(hre, contract);
    await owner.sendTransaction({ to: contribution.address, value: amount });
    console.log("Contribution is done.");
  });

task("get-all-contributors", "Get all contributors")
  .addParam("contract", "The contract address", undefined, types.address)
  .setAction(async (taskArgs, hre) => {
    const { contract } = taskArgs;
    const contribution = await getContributionContract(hre, contract);
    const result = await contribution.get_all_contributor();
    console.log("Contributor addresses:", result);
  });

task("get-contributor-total-sum", "Get total sum of contribution by address")
  .addParam("contract", "The contract address", undefined, types.address)
  .addParam(
    "contributorAddress",
    "Contributor address",
    undefined,
    types.address
  )
  .setAction(async (taskArgs, hre) => {
    const { contract, contributorAddress } = taskArgs;
    const contribution = await getContributionContract(hre, contract);
    const result = await contribution.get_contributor_total_sum(
      contributorAddress
    );
    console.log(
      "Contributor balance:",
      ethers.utils.formatUnits(result, "wei"),
      "Wei"
    );
  });

task("withdraw", "Withdraw money")
  .addParam("contract", "The contract address", undefined, types.address)
  .addParam("receiver", "Address to withdraw", undefined, types.address)
  .addParam("amount", "Amount for withdrawal", undefined, types.int)
  .setAction(async (taskArgs, hre) => {
    const { contract, receiver, amount } = taskArgs;
    [owner] = await ethers.getSigners();
    const contribution = await getContributionContract(hre, contract);
    await contribution.withdraw(receiver, amount);
    console.log("Withdrawal is done.");
  });
