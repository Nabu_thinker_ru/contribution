# Contribution contract 

![Coverage](https://camo.githubusercontent.com/2c8b15a3902bc15c0d1e6d70bbf7a1f0f248e2df4b430e25517c7543233530fb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f436f7665726167652d3130302532352d627269676874677265656e2e737667)

Simple contract to play with =)

## Installation & tests

Use npm to install all dependencies

```npm
npm install
```

To check tests coverage
```npm
npx hardhat coverage
```

## Configuration
To work with contract in Rinkeby network, you have to provide api keys in .env file in root project directory.

```npm
ALCHEMY_API_KEY='Your Alchemy API key goes here'
RINKEBY_API_KEY='Your Rinkeby API key goes here'
```

## Usage

```npm
# Deploy contract
npx hardhat --network rinkeby run scripts/deploy.js

# Make a contribution
npx hardhat --network rinkeby contribute --contract contract-address --amount amount-in-wei

# Get all contributors
npx hardhat --network rinkeby get-all-contributors --contract contract-address

# Get total sum of contribution by address
npx hardhat --network rinkeby get-contributor-total-sum --contract contract-address --contributor-address address

# Make a withdrawal
npx hardhat --network rinkeby withdraw --contract contract-address --receiver receiver-address --amount amount-in-wei
```

## Contributing ^_^
Pull requests are welcome. 
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)